#!/usr/bin/python3

"""Replaces std::string based string concatenation with cmStrCat.
   Distributed under the OSI-approved MIT License.
   See accompanying file Copyright.txt
"""

import os
import re
import sys
import shutil
import hashlib
import multiprocessing


class Options:
    def __init__(self):
        self.list_sources = False
        self.list_files = False
        self.list_files_inverted = False
        self.show_matches = False
        self.do_it_seriously = False


class File_Statistics:
    def __init__(self, filename):
        self.filename = filename

        self.expanded_list = 0


class Replacer:
    def __init__(self, options, stats):
        self.options = options
        self.stats = stats

        self.message = ""

    def replace(self, matchobj):
        grps = matchobj.groups()

        spacing = grps[0]
        vec_name = grps[1]
        str_name = grps[2]
        rest = grps[3]

        res = "\n" + spacing + "std::vector<std::string> " + vec_name + " = "
        res += "cmExpandedList(" + str_name + rest

        if (self.options.show_matches):
            self.message += "<< Match begin >>\n" + matchobj.group(
                0) + "\n<< Match end >>\n"
            self.message += "<< Result begin >>\n" + res + "\n<< Result end >>\n"

        return res


class Replacer_Basic(Replacer):
    def __init__(self, options, stats):
        super().__init__(options, stats)

        # Pattern
        self.pattern = "\n([ \t]*)std::vector<std::string>[\s]*(\w+);[\s]*\n"
        self.pattern += "\\1cmExpandList\\(([^;]+),[\s]*\\2[\s]*([,\)])"

        # Regular expression
        self.reg_exp = re.compile(self.pattern, re.DOTALL)


class File_Replacer:
    def __init__(self, filename, options):
        self.options = options
        self.stats = File_Statistics(filename)

        self.replacers = []
        self.replacers.append(Replacer_Basic(options, self.stats))

    def replace(self):
        if (self.options.show_matches):
            message = "Scanning " + self.stats.filename + "\n"

        # Read file into string
        with open(self.stats.filename, 'r') as fhandle:
            fdata = fhandle.read()

        is_fixed = False
        # Search and replace  repeatedly
        for rep in self.replacers:
            while (True):
                fdata, num_subs = rep.reg_exp.subn(rep.replace, fdata, count=1)
                if (num_subs == 0):
                    break
                if (self.options.show_matches):
                    message += rep.message
                is_fixed = True
                self.stats.expanded_list += 1

        # Write file
        if (is_fixed and self.options.do_it_seriously):
            self.write_file(fdata)

        # Debug messages
        if (self.options.show_matches):
            print(message, end='')

    def write_file(self, content):
        # Write temporary file and move to origin destination
        fname_hash = hashlib.sha256(self.stats.filename.encode()).hexdigest()
        tmp_file = "/tmp/cmStrCat_" + fname_hash + ".txt"
        with open(tmp_file, 'w') as fhandle:
            fhandle.write(content)
        shutil.move(tmp_file, self.stats.filename)


def replace(args):
    fname, options = args
    replacer = File_Replacer(fname, options)
    replacer.replace()
    return replacer.stats


if __name__ == '__main__':

    script_name = os.path.basename(__file__)
    script_dir = os.path.dirname(__file__)

    def exit_help():
        help_str = script_name
        help_str += '''\
CMAKE_SOURCE_DIR [OPTIONS]

OPTIONS:
    --help - Prints this help text
    --list-sources - List scanned files
    --list  - List fixed files
    --list-inverted - List not fixed files
    --show-matches - show matching strings
    --do-it - Actually change files
'''
        print(help_str)
        exit(-1)

    def exit_argument_error(text):
        print("Argument error: " + text)
        print("")
        exit_help()

    options = Options()

    source_dirs = []

    if (len(sys.argv) == 1):
        exit_help()

    if (len(sys.argv) > 1):
        if (sys.argv[1] == '--help'):
            exit_help()

        source_path = os.path.abspath(sys.argv[1])
        if (not os.path.isdir(source_path)):
            exit_argument_error("Invalid source directory: " + sys.argv[1])

        source_path += "/Source"
        if (not os.path.isdir(source_path)):
            exit_argument_error("Invalid source directory: " + source_path)

        source_dirs.append(source_path)

    if (len(sys.argv) > 2):
        opt = sys.argv[2]
        if (opt == '--help'):
            exit_help()
        elif (opt == '--list-sources'):
            options.list_sources = True
        elif (opt == '--list'):
            options.list_files = True
        elif (opt == '--list-inverted'):
            options.list_files_inverted = True
        elif (opt == '--show-matches'):
            options.list_files = True
            options.show_matches = True
        elif (opt == '--do-it'):
            options.do_it_seriously = True
        else:
            exit_argument_error("Unknown option: " + opt)

    file_list = []

    # Find files in the source directories
    for src_dir in source_dirs:
        for root, dirs, files in os.walk(src_dir):
            for fl in files:
                fl_full = root + "/" + fl
                fl_rel = os.path.relpath(fl_full, script_dir)
                file_list.append(fl_rel)
    file_list.sort()

    # Source file name filter
    file_filter_white = [
        re.compile(".*\.h$"),
        re.compile(".*\.hpp$"),
        re.compile(".*\.hxx$"),
        re.compile(".*\.cpp$"),
        re.compile(".*\.cxx$")
    ]

    file_filter_black = [
        re.compile(".*/Source/kwsys/.*"),
        re.compile(".*/Source/LexerParser/.*"),
        #re.compile ( ".*/Source/(?!cmQt).*" ),
    ]

    file_list_filtered = []

    for fname in file_list:
        white = False
        black = False
        for fl in file_filter_white:
            if (fl.match(fname)):
                white = True
                break
        for fl in file_filter_black:
            if (fl.match(fname)):
                black = True
                break
        if (white and not black):
            file_list_filtered.append(fname)

    # Debug messages
    if (options.list_sources):
        for fname in file_list_filtered:
            print(fname)
        exit(0)

    files_options = [(fname, options) for fname in file_list_filtered]

    num_processes = multiprocessing.cpu_count()
    if (not num_processes):
        num_processes = 4
    print("Number of processes: " + str(num_processes))

    mp_pool = multiprocessing.Pool(num_processes)
    files_stats = mp_pool.map(replace, [(fname, options)
                                        for fname in file_list_filtered])

    # Statistics
    class Statistics:
        def __init__(self):
            self.files_fixed = []
            self.files_not_fixed = []

            self.expanded_list = 0

        def compute(self, stats):
            for fstat in stats:
                if (fstat.expanded_list == 0):
                    self.files_not_fixed.append(fstat.filename)
                    continue
                # Fixed file
                self.files_fixed.append(fstat.filename)

                self.expanded_list += fstat.expanded_list

        def get_string(self):
            res = ""

            if (options.list_files):
                res += "\nFixed files:\n"
                for fname in sorted(self.files_fixed):
                    res += fname + "\n"

            if (options.list_files_inverted):
                res += "\nNot fixed files:\n"
                for fname in sorted(self.files_not_fixed):
                    res += fname + "\n"

            res += "\nStatistics:\n"
            res += "cmExpandList to cmExpandedList: " + str(
                self.expanded_list) + "\n"

            return res

    stats = Statistics()
    stats.compute(files_stats)

    print(stats.get_string())
