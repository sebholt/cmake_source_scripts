#!/usr/bin/python3

"""Replaces cmExpandList with cmExpandedList
   Distributed under the OSI-approved MIT License.
   See accompanying file Copyright.txt
"""

import os
import re
import sys
import shutil
import hashlib
import multiprocessing


# Append range
def append_range():
    app_min = 1
    app_max = 1
    return range(app_min, app_max + 1)


class Options:
    def __init__(self):
        self.list_sources = False
        self.list_files = False
        self.list_files_inverted = False
        self.show_matches = False
        self.do_it_seriously = False


class File_Statistics:
    def __init__(self, filename):
        self.filename = filename

        self.append_fixes = {}
        for ii in append_range():
            self.append_fixes[ii] = 0
        self.append_fixes_total = 0

        self.single_char_fixes = 0
        self.std_string_removes = 0
        self.std_to_string_removes = 0
        self.std_endl_replacements = 0
        self.substr_fixes = 0
        self.plus_separations = 0
        self.literal_arg_joins = 0


class Replacer:
    def __init__(self, append_count, options, stats):
        self.append_count = append_count
        self.options = options
        self.stats = stats

        # Patterns
        self.pat_spaced_literal = '(?:\\s*"(?:[^"\\\\]|\\\\.)*"\\s*)'
        self.pat_literal = '(?:"(?:[^"\\\\]|\\\\.)*")'
        self.re_literal = re.compile(self.pat_literal, re.DOTALL)

        self.pat_non_lt_bracket = '(?:[^<;])'
        self.pat_gt_brackets = '(?:<' + '[^<>;"]*' + '>)'
        self.pat_gt_brackets_n1 = '(?:<[^<>;]*<' + '<[^<>;]*' + '>[^<>;]*>)'
        self.pat_non_literal = '(?:[^"](?:' + self.pat_non_lt_bracket + '|' + self.pat_gt_brackets + '|' + self.pat_gt_brackets_n1 + ')+)'
        self.pat_argument = "(" + self.pat_spaced_literal + "|" + self.pat_non_literal + ")"
        self.pat_append = "<<" + self.pat_argument


        # Pattern
        self.pattern = "\n(?P<spacing>[ \t]*)  std::ostringstream[\\s]*(?P<var_name>\\w+)[ \t]*;[\\s]*\n"
        self.pattern += "(?P=spacing)  (?P=var_name)[\\s]*"
        for ii in range(self.append_count):
            self.pattern += self.pat_append
        self.pattern += ";[ ]*\n?"
        self.pattern += "(?P<post_def>.*?)\n(?P=spacing)}"


        # Regular expression
        self.reg_exp = re.compile(self.pattern, re.DOTALL)


        # Argument patterns
        pat_plus_left = "^((?:" + self.pat_literal + "|" + '(?:[^"\\+])' + ")+)\\+(.+)$"
        self.re_plus_left = re.compile(pat_plus_left, re.DOTALL)

        pat_plus_right = "^(.+)\\+((?:" + self.pat_literal + "|" + '(?:[^"\\+])' + ")+)$"
        self.re_plus_right = re.compile(pat_plus_right, re.DOTALL)

        pat_single_char = '^"([^"\\\\]|\\\\[abfnrtv0"\'\?\\\\])"$'
        self.re_single_char = re.compile(pat_single_char)

        pat_std_string = '^std::string\\((.+)\\)$'
        self.re_std_string = re.compile(pat_std_string)

        pat_std_to_string = '^std::to_string\\((.+)\\)$'
        self.re_std_to_string = re.compile(pat_std_to_string)

        pat_substr = '^(\w+)(\\.substr\\('
        pat_substr +=  '(?:' + '[^\\)]' + '|' + '(?:\\([^\\)]*\\))' + ')+'
        pat_substr += '\\))$'
        self.re_substr = re.compile(pat_substr)


        # Debug message
        self.message = ""


    def replace(self, matchobj):
        if (self.options.show_matches):
            self.message = ""

        spacing, varname, args = self.get_args(matchobj)
        complete_match = matchobj.group(0)
        post_def = matchobj.group('post_def')

        # Check if there are additional appends
        pat_extra_app = varname + "\\s*<<";
        if (re.search(pat_extra_app, post_def)):
            return complete_match

        # Check if the string is used raw
        pat_raw = "[^\\w\\.>]" + varname + "[^\\.]"
        if (re.search(pat_raw, post_def)):
            return complete_match

        # Check if there are unstringable arguments
        for arg in args:
            if(arg.find("rdbuf()") >= 0):
                return complete_match
            if(arg.find("std::setfill") >= 0):
                return complete_match
            if(arg.find("std::setw") >= 0):
                return complete_match
            if(arg.find("*this") >= 0):
                return complete_match


        # Debug messages
        if (self.options.show_matches):
            self.message += "<< Match begin >>\n" + matchobj.group(
                0) + "\n<< Match end >>\n"
            self.message += "<< Arguments begin >>\n"
            for ii in range(len(args)):
                self.message += "  " + str(ii) + ": " + str(args[ii]) + "\n"
            self.message += "<< Arguments end >>\n"


        var_def = ""
        if (len(args) > 1):
            var_def += "cmStrCat( " + (", ").join(args) + " )"
        else:
            var_def += args[0]

        # Inlineable regular expressions
        pat_var_dot_str = varname + ".str\\(\\)"
        #regs_inlineable = []
        #pat_single_function = "^(?P<PRE>[\\s]*[^;]+[^\\w\\.>])" + pat_var_dot_str + "(?P<POST>[\\s]*\\);[\s]*)$"
        #regs_inlineable.append(re.compile(pat_single_function))
        #pat_single_function_return = "^(?P<PRE>[\\s]*[^;]+[^\\w\\.>])" + pat_var_dot_str + "(?P<POST>[\\s]*\\);[\\s]*return[\\s\\w]*;[\s]*)$"
        #regs_inlineable.append(re.compile(pat_single_function_return))

        #pat_single_function_return = "^(?P<PRE>[\\s]*[^;]+[^\\w\\.>])" + pat_var_dot_str + "(?P<POST>[\\s]*\\);[\\s]*return[\\s\\w]*;[\s]*)$"
        #regs_inlineable.append(re.compile(pat_single_function_return))

        # Result
        res = "\n"

        inlined = False
        if (len(re.findall(pat_var_dot_str, post_def)) == 1):
            res += post_def.replace(varname + ".str()", var_def)
            inlined = True

        if(not inlined):
            res += spacing + "std::string const " + varname + " = "
            res += var_def + ";\n"
            res += re.sub( "([^\\w\\.>]?)" + pat_var_dot_str, "\\1" + varname, post_def, count=0, flags=re.DOTALL)
        res += "\n" + matchobj.group('spacing') + "}"


        # Debug messages
        if (self.options.show_matches):
            self.message += "<< Result begin >>\n" + res + "<< Result end >>\n"

        # Statistics
        self.stats.append_fixes[self.append_count] += 1
        self.stats.append_fixes_total += 1

        return res


    def get_args(self, matchobj):
        grps = matchobj.groups()
        #if (self.options.show_matches):
        #    self.message += "<< Groups begin >>\n"
        #    for ii in range(len(grps)):
        #        self.message += "  " + str(ii) + ": " + str(grps[ii]) + "\n"
        #    self.message += "<< Groups end >>\n"

        args = []
        for ii in range(2, len(grps) - 1):
            arg = grps[ii].strip()
            if (arg):
                args.append(arg)
        args = self.process_args(args)

        return grps[0] + "  ", grps[1], args


    def process_args(self, args):
        res = self.process_args_plus(args)
        res = self.process_args_literal(res)
        res = self.process_args_single_char(res)
        res = self.process_args_std_to_string(res)
        res = self.process_args_std_string(res)
        res = self.process_args_std_endl(res)
        res = self.process_args_substr(res)
        return res


    def process_args_plus(self, args):
        res = []
        for arg in args:
            pleft = []
            while (arg):
                mo = self.re_plus_left.match(arg)
                if (not mo):
                    break
                narg = mo.group(1).strip()
                if (narg.count('(') != narg.count(')')):
                    break
                pleft.append(narg)
                arg = mo.group(2).strip()

            pright = []
            while (arg):
                mo = self.re_plus_right.match(arg)
                if (not mo):
                    break
                narg = mo.group(2).strip()
                if (narg.count('(') != narg.count(')')):
                    break
                pright.insert(0, narg)
                arg = mo.group(1).strip()

            res.extend(pleft)
            if (arg):
                res.append(arg)
            res.extend(pright)

            # Statistics
            if (pleft or pright):
                self.stats.plus_separations += 1

        return res


    def process_args_literal(self, args):
        res = []
        while (args):
            if (self.re_literal.match(args[0])):
                narg = args[0].strip()
                args.pop(0)
                combined = 1
                while (args and self.re_literal.match(args[0])):
                    narg = narg[:-1] + args[0].strip()[1:]
                    args.pop(0)
                    combined += 1
                if(combined > 1):
                    self.stats.literal_arg_joins += 1
                #    print ( "Combined(" + str(combined) + "): " + narg )
                res.append(narg)
            else:
                res.append(args[0].strip())
                args.pop(0)
        return res


    def process_args_single_char(self, args):
        res = []
        for arg in args:
            mo = self.re_single_char.search(arg)
            if (mo):
                char = mo.group(1)
                if (char == '\\"'):
                    arg = "'\"'"
                elif (char == "'"):
                    arg = "'\\''"
                else:
                    arg = "'" + char + "'"
                self.stats.single_char_fixes += 1
                #print ( "Single char: " + arg )
            res.append(arg)
        return res


    def process_args_std_to_string(self, args):
        if (len(args) < 2):
            return args

        res = []
        for arg in args:
            while (True):
                mo = self.re_std_to_string.search(arg)
                if (mo):
                    arg = mo.group(1).strip()
                    self.stats.std_to_string_removes += 1
                    continue
                break

            if (arg):
                res.append(arg)
        return res


    def process_args_std_string(self, args):
        res = []
        for arg in args:
            while (True):
                mo = self.re_std_string.search(arg)
                if (mo):
                    arg = mo.group(1).strip()
                    self.stats.std_string_removes += 1
                    continue
                break

            if (arg):
                res.append(arg)
        return res


    def process_args_std_endl(self, args):
        res = []
        for arg in args:
            if (arg == "std::endl" ):
                arg = "'\\n'"
                self.stats.std_endl_replacements += 1
            if (arg):
                res.append(arg)
        return res


    def process_args_substr(self, args):
        res = []
        for arg in args:
            mo = self.re_substr.search(arg)
            if (mo):
                arg = "cm::string_view(" + mo.group(1).strip() + ")" + mo.group(2).strip()
                self.stats.substr_fixes += 1

            res.append(arg)
        return res


class File_Replacer:
    def __init__(self, filename, options):
        self.options = options
        self.stats = File_Statistics(filename)

        self.replacers = {}
        for ii in append_range():
            args = (ii, options, self.stats)
            reps = []
            reps.append(Replacer(*args))
            self.replacers[ii] = reps

    def replace(self):
        if (self.options.show_matches):
            message = "Scanning " + self.stats.filename + "\n"

        # Read file into string
        with open(self.stats.filename, 'r') as fhandle:
            fdata = fhandle.read()

        is_fixed = False
        # Search and replace  repeatedly
        for ii in append_range():
            for rep in self.replacers[ii]:
                while (True):
                    fdata_before = fdata
                    fdata, num_subs = rep.reg_exp.subn(rep.replace,
                                                       fdata,
                                                       count=0)
                    if (num_subs == 0):
                        break
                    if (fdata == fdata_before):
                        break
                    if (self.options.show_matches):
                        message += rep.message
                    is_fixed = True

        # Write file
        if (is_fixed and self.options.do_it_seriously):
            self.write_file(fdata)

        # Debug messages
        if (self.options.show_matches):
            print(message, end='')

    def write_file(self, content):
        # Write temporary file and move to origin destination
        fname_hash = hashlib.sha256(self.stats.filename.encode()).hexdigest()
        tmp_file = "/tmp/cmStrCat_" + fname_hash + ".txt"
        with open(tmp_file, 'w') as fhandle:
            fhandle.write(content)
        shutil.move(tmp_file, self.stats.filename)


def replace(args):
    fname, options = args
    replacer = File_Replacer(fname, options)
    replacer.replace()
    return replacer.stats


if __name__ == '__main__':

    script_name = os.path.basename(__file__)
    script_dir = os.path.dirname(__file__)

    def exit_help():
        help_str = script_name
        help_str += '''\
CMAKE_SOURCE_DIR [OPTIONS]

OPTIONS:
    --help - Prints this help text
    --list-sources - List scanned files
    --list  - List fixed files
    --list-inverted - List not fixed files
    --show-matches - show matching strings
    --do-it - Actually change files
'''
        print(help_str)
        exit(-1)

    def exit_argument_error(text):
        print("Argument error: " + text)
        print("")
        exit_help()

    options = Options()

    source_dirs = []

    if (len(sys.argv) == 1):
        exit_help()

    if (len(sys.argv) > 1):
        if (sys.argv[1] == '--help'):
            exit_help()

        source_path = os.path.abspath(sys.argv[1])
        if (not os.path.isdir(source_path)):
            exit_argument_error("Invalid source directory: " + sys.argv[1])

        source_path += "/Source"
        if (not os.path.isdir(source_path)):
            exit_argument_error("Invalid source directory: " + source_path)

        source_dirs.append(source_path)

    if (len(sys.argv) > 2):
        opt = sys.argv[2]
        if (opt == '--help'):
            exit_help()
        elif (opt == '--list-sources'):
            options.list_sources = True
        elif (opt == '--list'):
            options.list_files = True
        elif (opt == '--list-inverted'):
            options.list_files_inverted = True
        elif (opt == '--show-matches'):
            options.list_files = True
            options.show_matches = True
        elif (opt == '--do-it'):
            options.do_it_seriously = True
        else:
            exit_argument_error("Unknown option: " + opt)

    file_list = []

    # Find files in the source directories
    for src_dir in source_dirs:
        for root, dirs, files in os.walk(src_dir):
            for fl in files:
                fl_full = root + "/" + fl
                fl_rel = os.path.relpath(fl_full, script_dir)
                file_list.append(fl_rel)
    file_list.sort()

    # Source file name filter
    file_filter_white = [
        re.compile(".*\.h$"),
        re.compile(".*\.hpp$"),
        re.compile(".*\.hxx$"),
        re.compile(".*\.cpp$"),
        re.compile(".*\.cxx$")
    ]

    file_filter_black = [
        re.compile(".*/Source/kwsys/.*"),
        re.compile(".*/Source/LexerParser/.*"),
        #re.compile ( ".*/Source/(?!cmQt).*" ),
        #re.compile(".*/Source/(?!CPack/cmCPackBundle).*"),
    ]

    file_list_filtered = []

    for fname in file_list:
        white = False
        black = False
        for fl in file_filter_white:
            if (fl.match(fname)):
                white = True
                break
        for fl in file_filter_black:
            if (fl.match(fname)):
                black = True
                break
        if (white and not black):
            file_list_filtered.append(fname)

    # Debug messages
    if (options.list_sources):
        for fname in file_list_filtered:
            print(fname)
        exit(0)

    files_options = [(fname, options) for fname in file_list_filtered]

    num_processes = multiprocessing.cpu_count()
    if (not num_processes):
        num_processes = 4
    print("Number of processes: " + str(num_processes))

    mp_pool = multiprocessing.Pool(num_processes)
    files_stats = mp_pool.map(replace, [(fname, options)
                                        for fname in file_list_filtered])

    # Statistics
    class Statistics:
        def __init__(self):
            self.files_fixed = []
            self.files_not_fixed = []

            self.append_fixes = {}
            for ii in append_range():
                self.append_fixes[ii] = 0
            self.append_fixes_total = 0

            self.plus_separations = 0
            self.literal_arg_joins = 0

            self.single_char_fixes = 0
            self.std_string_removes = 0
            self.std_to_string_removes = 0
            self.std_endl_replacements = 0
            self.substr_fixes = 0


        def compute(self, stats):
            for fstat in stats:
                if (fstat.append_fixes_total == 0):
                    self.files_not_fixed.append(fstat.filename)
                    continue
                # Fixed file
                self.files_fixed.append(fstat.filename)

                for ii in append_range():
                    self.append_fixes[ii] += fstat.append_fixes[ii]
                self.append_fixes_total += fstat.append_fixes_total

                self.plus_separations += fstat.plus_separations
                self.literal_arg_joins += fstat.literal_arg_joins

                self.single_char_fixes += fstat.single_char_fixes
                self.std_string_removes += fstat.std_string_removes
                self.std_to_string_removes += fstat.std_to_string_removes
                self.std_endl_replacements += fstat.std_endl_replacements
                self.substr_fixes += fstat.substr_fixes


        def get_string(self):
            res = ""

            if (options.list_files):
                res += "\nFixed files:\n"
                for fname in sorted(self.files_fixed):
                    res += fname + "\n"

            if (options.list_files_inverted):
                res += "\nNot fixed files:\n"
                for fname in sorted(self.files_not_fixed):
                    res += fname + "\n"

            res += "\nStatistics:\n"
            res += "Plus separations: " + str(self.plus_separations) + "\n"
            res += "Literal arguments joins: " + str(self.literal_arg_joins) + "\n"
            res += "Single char string fixes: " + str(
                self.single_char_fixes) + "\n"
            res += "std::string removes: " + str(
                self.std_string_removes) + "\n"
            res += "std::to_string removes: " + str(
                self.std_to_string_removes) + "\n"
            res += "std::endl replacements: " + str(
                self.std_endl_replacements) + "\n"
            res += "cm::string_view substr replacements: " + str(
                self.substr_fixes) + "\n"

            res += "String multiple append fixes:\n"
            for ii in append_range():
                if (self.append_fixes[ii] != 0):
                    res += "  " + str(ii) + ": " + str(
                        self.append_fixes[ii]) + "\n"
            res += "String append fixes total: " + str(
                self.append_fixes_total) + "\n"

            return res

    stats = Statistics()
    stats.compute(files_stats)

    print(stats.get_string())
